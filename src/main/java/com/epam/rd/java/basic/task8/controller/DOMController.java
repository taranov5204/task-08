package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.entity.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static com.epam.rd.java.basic.task8.Constant.FLOWER;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private String xmlFileName;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public List<Flower> parse() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbf =
                DocumentBuilderFactory.newInstance();

        dbf.setNamespaceAware(true);

        dbf.setFeature("http://xml.org/sax/features/validation", true);
        dbf.setFeature("http://apache.org/xml/features/validation/schema", true);
        dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);

        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(xmlFileName);

        Element root = doc.getDocumentElement();
        List<Flower> flowers = new LinkedList<>();

        NodeList flowerNodes =
                root.getElementsByTagName(FLOWER);

        for (int j = 0; j < flowerNodes.getLength(); j++) {
            Flower flower = getFlower(flowerNodes.item(j));
            flowers.add(flower);
        }
        return flowers;
    }

    private Flower getFlower(Node node) {
        NodeList childNodes = node.getChildNodes();
        String name = childNodes.item(1).getTextContent();
        String soil = childNodes.item(3).getTextContent();
        String origin = childNodes.item(5).getTextContent();
        String multiplying = childNodes.item(11).getTextContent();

        NodeList visualParamList = childNodes.item(7).getChildNodes();
        VisualParameters visualParameters = getVisualParam(visualParamList);

        NodeList growingTipsList = childNodes.item(9).getChildNodes();
        GrowingTips growingTips = getGrowingTips(growingTipsList);

        return new Flower(name, soil, origin, visualParameters, growingTips, multiplying);
    }

    private VisualParameters getVisualParam(NodeList visualParamList) {
        String stemColor = visualParamList.item(1).getTextContent();
        String leafColor = visualParamList.item(3).getTextContent();
        String avgLenText = visualParamList.item(5).getTextContent();
        String localName = visualParamList.item(5).getAttributes().item(0).getLocalName();
        String localNameVal = visualParamList.item(5).getAttributes().item(0).getTextContent();
        return new VisualParameters(stemColor, leafColor, new LocalObject(localName, localNameVal, avgLenText));
    }

    private GrowingTips getGrowingTips(NodeList growingTipsList) {
        String temperature = growingTipsList.item(1).getTextContent();
        String localName = growingTipsList.item(1).getAttributes().item(0).getLocalName();
        String localNameVal = growingTipsList.item(1).getAttributes().item(0).getTextContent();
        LocalObject temperatureObj = new LocalObject(localName, localNameVal, temperature);

        localName = growingTipsList.item(3).getAttributes().item(0).getLocalName();
        localNameVal = growingTipsList.item(3).getAttributes().item(0).getTextContent();
        LocalObject lightningObj = new LocalObject(localName, localNameVal);

        String watering = growingTipsList.item(5).getTextContent();
        localName = growingTipsList.item(5).getAttributes().item(0).getLocalName();
        localNameVal = growingTipsList.item(5).getAttributes().item(0).getTextContent();
        LocalObject wateringObj = new LocalObject(localName, localNameVal, watering);

        return new GrowingTips(temperatureObj, lightningObj, wateringObj);
    }


}
