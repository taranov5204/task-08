package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Constant;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.LocalObject;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;

import static com.epam.rd.java.basic.task8.Constant.*;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private String xmlFileName;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE
    public List<Flower> parse() throws ParserConfigurationException, FileNotFoundException, XMLStreamException {
        DocumentBuilderFactory dbf =
                DocumentBuilderFactory.newInstance();

        dbf.setNamespaceAware(true);

        XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
        xmlInputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
        XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));

        List<Flower> parseResult = parseXml(reader);

        return parseResult;
    }

    private List<Flower> parseXml(XMLEventReader reader) throws XMLStreamException {
        List<Flower> flowers = new LinkedList<>();

        Flower flower = null;
        VisualParameters visualParameters = null;
        GrowingTips growingTips = null;
        LocalObject localObject = null;

        while (reader.hasNext()) {
            XMLEvent nextEvent = reader.nextEvent();
            if (nextEvent.isStartElement()) {
                StartElement startElement = nextEvent.asStartElement();
                String elName = startElement.getName().getLocalPart();
                switch (elName) {
                    case Constant.FLOWER:
                        flower = new Flower();
                        visualParameters = new VisualParameters();
                        growingTips = new GrowingTips();
                        break;
                    case NAME:
                        nextEvent = reader.nextEvent();
                        flower.setName(nextEvent.asCharacters().getData());
                        break;
                    case SOIL:
                        nextEvent = reader.nextEvent();
                        flower.setSoil(nextEvent.asCharacters().getData());
                        break;
                    case ORIGIN:
                        nextEvent = reader.nextEvent();
                        flower.setOrigin(nextEvent.asCharacters().getData());
                        break;
                    case MULTIPLYING:
                        nextEvent = reader.nextEvent();
                        flower.setMultiplying(nextEvent.asCharacters().getData());
                        break;
                    case STEM_COLOUR:
                        visualParameters = new VisualParameters();
                        nextEvent = reader.nextEvent();
                        visualParameters.setStemColour(nextEvent.asCharacters().getData());
                        break;
                    case LEAF_COLOR:
                        nextEvent = reader.nextEvent();
                        visualParameters.setLeafColour(nextEvent.asCharacters().getData());
                        break;
                    case AVERAGE_LENGTH:
                    case LIGHTING:
                    case TEMPRETURE:
                    case WATERING:
                        nextEvent = reader.nextEvent();
                        Attribute attr = startElement.getAttributes().next();
                        boolean isExist = nextEvent.isCharacters();
                        String text = isExist ? nextEvent.asCharacters().getData() : null;
                        String name = attr.getName().getLocalPart();
                        String nameVal = attr.getValue();
                        localObject = new LocalObject(name, nameVal);
                        if (isExist) localObject.setText(text);
                        if (elName.equals(LIGHTING)) growingTips.setLighting(localObject);
                        break;
                    default:
                        break;
                }
            } else if (nextEvent.isEndElement()) {
                EndElement endElement = nextEvent.asEndElement();
                String elName = endElement.getName().getLocalPart();
                switch (elName) {
                    case AVERAGE_LENGTH:
                        visualParameters.setAveLenFlower(localObject);
                        break;
                    case TEMPRETURE:
                        growingTips.setTempreture(localObject);
                        break;
                    case WATERING:
                        growingTips.setWatering(localObject);
                        break;
                    case FLOWER:
                        flower.setGrowingTips(growingTips);
                        flower.setVisualParameters(visualParameters);
                        flowers.add(flower);
                    default:
                        break;

                }
            }
        }
        return flowers;
    }


}