package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.VisualParameters;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CreateOutFile {

    private CreateOutFile() {
    }

    public static void create(String outputXmlFile, List<Flower> flowerList) {
        File outFile = new File(outputXmlFile);
        XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
        XMLStreamWriter streamWriter = null;

        try (FileWriter fw = new FileWriter(outFile)) {
            streamWriter = xmlOutputFactory.createXMLStreamWriter(fw);

            //write start
            streamWriter.writeStartDocument("UTF-8", "1.0");
            streamWriter.writeStartElement("flowers");
            streamWriter.writeAttribute("xmlns", "http://www.nure.ua");
            streamWriter.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            streamWriter.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");

            //flower write
            for (Flower flower : flowerList) {
                writeFlower(streamWriter, flower);
            }
            streamWriter.writeEndElement();

        } catch (IOException | XMLStreamException e) {
            e.printStackTrace();
        }
    }

    private static void writeFlower(XMLStreamWriter streamWriter, Flower flower) throws XMLStreamException {
        streamWriter.writeStartElement("flower");

        streamWriter.writeStartElement("name");
        streamWriter.writeCharacters(flower.getName());
        streamWriter.writeEndElement();

        streamWriter.writeStartElement("soil");
        streamWriter.writeCharacters(flower.getSoil());
        streamWriter.writeEndElement();

        streamWriter.writeStartElement("origin");
        streamWriter.writeCharacters(flower.getOrigin());
        streamWriter.writeEndElement();

        writeVisualParam(streamWriter, flower.getVisualParameters());

        writeGrowingTips(streamWriter, flower.getGrowingTips());

        streamWriter.writeStartElement("multiplying");
        streamWriter.writeCharacters(flower.getMultiplying());
        streamWriter.writeEndElement();

        streamWriter.writeEndElement();
    }

    private static void writeVisualParam(XMLStreamWriter streamWriter, VisualParameters visualParameters) throws XMLStreamException {
        streamWriter.writeStartElement("visualParameters");

        streamWriter.writeStartElement("stemColour");
        streamWriter.writeCharacters(visualParameters.getStemColour());
        streamWriter.writeEndElement();

        streamWriter.writeStartElement("leafColour");
        streamWriter.writeCharacters(visualParameters.getLeafColour());
        streamWriter.writeEndElement();

        streamWriter.writeStartElement("aveLenFlower");
        String localName = visualParameters.getAveLenFlower().getLocalName();
        String localVal = visualParameters.getAveLenFlower().getLocalValue();
        String text = visualParameters.getAveLenFlower().getText();
        streamWriter.writeAttribute(localName, localVal);
        streamWriter.writeCharacters(text);
        streamWriter.writeEndElement();

        streamWriter.writeEndElement();
    }

    private static void writeGrowingTips(XMLStreamWriter streamWriter, GrowingTips growingTips) throws XMLStreamException {
        streamWriter.writeStartElement("growingTips");

        //write temperature
        streamWriter.writeStartElement("tempreture");
        String localName = growingTips.getTempreture().getLocalName();
        String localVal = growingTips.getTempreture().getLocalValue();
        String text = growingTips.getTempreture().getText();
        streamWriter.writeAttribute(localName, localVal);
        streamWriter.writeCharacters(text);
        streamWriter.writeEndElement();

        // write lightning
        streamWriter.writeEmptyElement("lighting");
        localName = growingTips.getLighting().getLocalName();
        localVal = growingTips.getLighting().getLocalValue();
        streamWriter.writeAttribute(localName, localVal);

        // write watering
        streamWriter.writeStartElement("watering");
        localName = growingTips.getWatering().getLocalName();
        localVal = growingTips.getWatering().getLocalValue();
        text = growingTips.getWatering().getText();
        streamWriter.writeAttribute(localName, localVal);
        streamWriter.writeCharacters(text);
        streamWriter.writeEndElement();

        streamWriter.writeEndElement();
    }
}
