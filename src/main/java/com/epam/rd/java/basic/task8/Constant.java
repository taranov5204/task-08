package com.epam.rd.java.basic.task8;

public class Constant {

    public static final String AVERAGE_LENGTH = "aveLenFlower";
    public static final String TEMPRETURE = "tempreture";
    public static final String WATERING = "watering";
    public static final String LIGHTING = "lighting";
    public static final String NAME = "name";
    public static final String SOIL = "soil";
    public static final String ORIGIN = "origin";
    public static final String MULTIPLYING = "multiplying";
    public static final String STEM_COLOUR = "stemColour";
    public static final String LEAF_COLOR = "leafColour";
    public static final String FLOWER = "flower";
}
