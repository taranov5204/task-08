package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flower;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		List<Flower> flowerList = domController.parse();


		// sort (case 1)
		// PLACE YOUR CODE HERE
		flowerList.sort(Comparator.comparing(Flower::getName));
		
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE

		CreateOutFile.create(outputXmlFile, flowerList);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		flowerList = saxController.parse();
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		flowerList.sort(Comparator.comparing(o -> o.getVisualParameters().getAveLenFlower().getText()));
		Collections.reverse(flowerList);
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		CreateOutFile.create(outputXmlFile, flowerList);
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		flowerList = staxController.parse();
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		flowerList.sort(Comparator.comparing(Flower::getOrigin));
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		CreateOutFile.create(outputXmlFile, flowerList);
	}

}
