package com.epam.rd.java.basic.task8.entity;

public class GrowingTips {

    LocalObject tempreture;
    LocalObject lighting;
    LocalObject watering;

    public GrowingTips() {
    }

    public GrowingTips(LocalObject tempreture, LocalObject lighting, LocalObject watering) {
        this.tempreture = tempreture;
        this.lighting = lighting;
        this.watering = watering;
    }

    public LocalObject getTempreture() {
        return tempreture;
    }

    public void setTempreture(LocalObject tempreture) {
        this.tempreture = tempreture;
    }

    public LocalObject getLighting() {
        return lighting;
    }

    public void setLighting(LocalObject lighting) {
        this.lighting = lighting;
    }

    public LocalObject getWatering() {
        return watering;
    }

    public void setWatering(LocalObject watering) {
        this.watering = watering;
    }
}
