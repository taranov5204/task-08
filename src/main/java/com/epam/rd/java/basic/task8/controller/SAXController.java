package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Constant;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.LocalObject;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static com.epam.rd.java.basic.task8.Constant.*;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private String xmlFileName;
    static List<Flower> flowers = new LinkedList<>();

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public List<Flower> parse() throws SAXException, ParserConfigurationException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(true);

        factory.setFeature("http://xml.org/sax/features/validation", true);
        factory.setFeature("http://apache.org/xml/features/validation/schema", true);
        factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);

        SAXParser parser = factory.newSAXParser();

        XmlHandler xmlHandler = new XmlHandler();

        parser.parse("input.xml", xmlHandler);
        return flowers;
    }

    private static class XmlHandler extends DefaultHandler {

        Flower flower;
        VisualParameters visualParameters;
        GrowingTips growingTips;
        LocalObject localObject;
        String currentVal;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            currentVal = localName;

            switch (localName) {
                case AVERAGE_LENGTH:
                case TEMPRETURE:
                case WATERING:
                case LIGHTING:
                    localObject = new LocalObject();
                    localObject.setLocalName(attributes.getLocalName(0));
                    localObject.setLocalValue(attributes.getValue(0));
                    break;
                default:
                    break;
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            switch (currentVal) {
                case TEMPRETURE:
                    growingTips.setTempreture(localObject);
                    break;
                case LIGHTING:
                    growingTips.setLighting(localObject);
                    break;
                case WATERING:
                    growingTips.setWatering(localObject);
                    break;
                case AVERAGE_LENGTH:
                    visualParameters.setAveLenFlower(localObject);
                    break;
                default:
                    break;
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            String content =
                    new String(ch, start, length).trim();

            if (content.isEmpty()) {
                return;
            }

            switch (currentVal) {
                case NAME:
                    flower = new Flower();
                    growingTips = new GrowingTips();
                    visualParameters = new VisualParameters();
                    flower.setName(content);
                    break;
                case SOIL:
                    flower.setSoil(content);
                    break;
                case ORIGIN:
                    flower.setOrigin(content);
                    break;
                case MULTIPLYING:
                    flower.setMultiplying(content);
                    flower.setVisualParameters(visualParameters);
                    flower.setGrowingTips(growingTips);
                    flowers.add(flower);
                    break;
                case STEM_COLOUR:
                    visualParameters.setStemColour(content);
                    break;
                case LEAF_COLOR:
                    visualParameters.setLeafColour(content);
                    break;
                case AVERAGE_LENGTH:
                case TEMPRETURE:
                case WATERING:
                    localObject.setText(content);
                    break;
                default:
                    break;
            }
        }
    }
}