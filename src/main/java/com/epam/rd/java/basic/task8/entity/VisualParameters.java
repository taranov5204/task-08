package com.epam.rd.java.basic.task8.entity;

public class VisualParameters {
    private String stemColour;
    private String leafColour;
    private LocalObject aveLenFlower;

    public VisualParameters() {
    }

    public VisualParameters(String stemColour, String leafColour, LocalObject aveLenFlower) {
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.aveLenFlower = aveLenFlower;
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public LocalObject getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(LocalObject aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }
}
