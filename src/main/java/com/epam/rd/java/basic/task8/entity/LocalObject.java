package com.epam.rd.java.basic.task8.entity;

public class LocalObject {

    private String text;
    private String localName;
    private String localValue;

    public LocalObject(String localName, String localValue, String text) {
        this.localName = localName;
        this.localValue = localValue;
        this.text = text;
    }

    public LocalObject() {
    }

    public LocalObject(String localName, String localValue) {
        this.localName = localName;
        this.localValue = localValue;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    public String getLocalValue() {
        return localValue;
    }

    public void setLocalValue(String localValue) {
        this.localValue = localValue;
    }
}
